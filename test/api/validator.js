const Code = require('code');
const Lab = require('lab');
const Joi = require('joi');
const Validator = require('../../src/api/validator');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const expect = Code.expect;

describe('study schema', () => {
    it('should return {error: null, value: ...} when passed valid study', (done) => {
        const study = {
            identifier: 'S123',
            recordType: 'Study',
            lastModified: '1970-01-01T01:00:00Z',
            persistentIdentifier: 'doi:10.3886/ICPSR09212.v3',
            downloadURL: [{ fileFormat: 'csv', URL: 'http://example.com/Download/S123.csv' }],
            orderURL: 'http://example.com/Order/S123',
            landingPageURL: 'http://example.com/Study/S123',
            prefLabel: {
                en: 'Some label',
                no: 'En merkelapp',
            },
            title: {
                en: 'Some title',
            },
            subtitle: {
                en: 'Some sub title',
            },
            alternative: {
                en: 'Some alternative string',
            },
            abstract: {
                en: 'Some abstract',
            },
            analysisUnit: {
                en: 'Person',
            },
            ddiFile: [
                'DDIFile1',
                'DDIFile2',
            ],
            fundedBy: [
                { en: 'organization1' },
                { en: 'organization2' },
            ],
            inGroup: [
                'studyGroupId1',
                'studyGroupId2',
            ],
            instrument: [
                'Instrument1',
                'Instrument2',
            ],
            kindOfData: {
                en: 'Kind of data',
            },
            subject: [
                {
                    en: 'Subject 1',
                    no: 'Emne 1',
                },
                {
                    en: 'Subject 2',
                    no: 'Emne 2',
                },
            ],
            product: 'LogicalDataset', // ID of logical dataset file
            purpose: {
                en: 'Some purpose',
            },
            universe: {
                en: 'Universe description for study',
            },
            variable: [
                'varid1',
                'varid2',
            ],
            available: '1970-01-01T01:00:00.000Z',
            temporal: {
                startDate: '1970-01-01T01:00:00.000Z',
                endDate: '1971-01-01T01:00:00.000Z',
            },
            spatial: [
                {
                    en: 'Norway',
                },
                {
                    en: 'Sweden',
                },
            ],
        }; // End of study object

        const validatedStudy = Joi.validate(study, Validator.studySchema);
        expect(validatedStudy.error).to.equal(null);
        done();
    });

    it('should accept null and empty array/object for values other than identifier', (done) => {
        const study = {
            identifier: 'Some identifier',
            recordType: 'Study',
            lastModified: '1970-01-01T01:00:00Z',
            persistentIdentifier: null,
            downloadURL: [],
            orderURL: null,
            landingPageURL: null,
            prefLabel: {},
            title: {},
            subtitle: {},
            alternative: {},
            abstract: {},
            analysisUnit: {},
            ddiFile: [],
            fundedBy: [],
            inGroup: [],
            instrument: [],
            kindOfData: {},
            subject: [],
            product: null, // ID of logical dataset file
            purpose: {},
            universe: {},
            variable: [],
            available: null,
            temporal: {
                startDate: null,
                endDate: null,
            },
            spatial: [],
        }; // End of study object

        const validatedStudy = Joi.validate(study, Validator.studySchema);
        expect(validatedStudy.error).to.equal(null);
        done();
    });

    it('should return { error: "...."} when passed invalid study object', (done) => {
        const study = {};
        const validatedStudy = Joi.validate(study, Validator.studySchema, { abortEarly: false });

        expect(validatedStudy).to.contain('error');
        expect(validatedStudy.error.name).to.equal('ValidationError');

        done();
    });
});

describe('variable schema', () => {
    it('should return {error: null, value: ...} when passed valid study', (done) => {
        const variable = {
            identifier: 'Var1',
            recordType: 'Variable',
            lastModified: '1970-01-01T01:00:00Z',
            persistentIdentifier: 'doi:10.3886/ICPSR09212.v3',
            prefLabel: {
                en: 'Some label',
                no: 'En merkelapp',
            },
            description: {
                en: 'Some title',
            },
            notation: 'The variable name',
            basedOn: 'Id of represented variable?',
            analysisUnit: {
                en: 'Person',
            },
            subject: [
                {
                    en: 'Subject 1',
                },
            ],
            concept: {
                en: 'Variable concept',
            },
            question: [
                'questionid1',
                'questionid2',
            ],
            inStudy: ['some_study'],
            representation: 'string',
            codeList: [
                {
                    prefLabel: {
                        en: 'Male',
                        no: 'Mann',
                    },
                    notation: '1',
                },
                {
                    prefLabel: {
                        en: 'Female',
                    },
                    notation: '2',
                },
            ],
            universe: {
                en: 'Same description as study?',
            },
        }; // End of variable

        const validatedVariable = Joi.validate(variable, Validator.variableSchema);
        expect(validatedVariable.error).to.equal(null);

        done();
    });

    it('should accept null and empty array/object for values other than identifier', (done) => {
        const variable = {
            identifier: 'Var1',
            recordType: 'Variable',
            lastModified: '1970-01-01T01:00:00Z',
            // prefLabel: {}, supports missing properties as well
            // description: {},
            persistentIdentifier: null,
            subject: [],
            notation: null,
            basedOn: null,
            analysisUnit: {},
            concept: {},
            question: [],
            representation: null,
            codeList: [],
            inStudy: [],
            universe: {},
        }; // End of variable

        const validatedVariable = Joi.validate(variable, Validator.variableSchema);
        expect(validatedVariable.error).to.equal(null);

        done();
    });
});

describe('record headers schema', () => {
    it('should not return error when passed valid record headers list', (done) => {
        const recordHeadersList = [
            {
                identifier: 'NSD1234',
                recordType: 'RecordHeader',
                type: 'Study',
                lastModified: '2000-01-01T01:00:00.000Z',
            },
        ];

        const validatedRecordHeaders =
            Joi.validate(recordHeadersList, Validator.recordHeadersListSchema);

        expect(validatedRecordHeaders.error).to.equal(null);
        done();
    });

    it('should return validation error when passed invalid record headers list', (done) => {
        const recordHeadersList = [
            {
                identifier: 'NSD1234',
                typo: 'Study',
                lastModified: '2000-01-01T01:00:00.000Z',
            },
        ];

        const validatedRecordHeadersList =
            Joi.validate(recordHeadersList, Validator.recordHeadersListSchema);

        expect(validatedRecordHeadersList.error).to.exist();

        const recordHeadersListExtraProp = [
            {
                identifier: 'NSD1234',
                type: 'Study',
                lastModified: '2000-01-01T01:00:00.000Z',
                prop: 'extra',
            },
        ];

        const validatedRecordHeadersListExtraProp =
            Joi.validate(recordHeadersListExtraProp, Validator.recordHeadersListSchema);

        expect(validatedRecordHeadersListExtraProp.error).to.exist();

        done();
    });
});

describe('question schema validation', () => {
    it('should return {error: null, value: ...} when passed a valid question', (done) => {
        const question = {
            identifier: 'Question1',
            recordType: 'Question',
            lastModified: '1970-01-01T01:00:00Z',
            persistentIdentifier: 'doi:10.3886/ICPSR09212.v3',
            questionText: {
                en: 'My question',
            },
            inVariable: ['some_variable'],
            representation: 'string',
            codeList: [
                {
                    prefLabel: {
                        en: 'Male',
                        no: 'Mann',
                    },
                    notation: '1',
                },
                {
                    prefLabel: {
                        en: 'Female',
                    },
                    notation: '2',
                },
            ],
        };

        const validatedQuestion = Joi.validate(question, Validator.questionSchema);
        expect(validatedQuestion.error).to.be.null();

        done();
    });

    it('should accept null and empty array/object for values other than identifier', (done) => {
        const question = {
            identifier: 'Question1',
            recordType: 'Question',
            lastModified: '1970-01-01T01:00:00Z',
            persistentIdentifier: null,
            questionText: {},
            inVariable: [],
            representation: null,
            codeList: [],
        };

        const validatedQuestion = Joi.validate(question, Validator.questionSchema);
        expect(validatedQuestion.error).to.be.null();

        done();
    });

    it('should return { error: "..." } when passed invalid question', (done) => {
        const question = {
            identifier: 'Question1',
            recordType: 'Question',
            persistentIdentifier: 'doi:10.3886/ICPSR09212.v3',
            questionText: {
                en: 'My question',
            },
            responseDomain: ['domain1', 'domain2'],
            representation: 'string',
            codeList: [
                {
                    prefLabel: {
                        en: 'Male',
                        no: 'Mann',
                    },
                    notation: '1',
                },
                {
                    prefLabel: {
                        en: 'Female',
                    },
                    notatine: '2', /* typo in 'notatine' should be detected */
                },
            ],
        };

        const validatedQuestion = Joi.validate(question, Validator.questionSchema);
        expect(validatedQuestion.error).to.not.be.null();
        expect(validatedQuestion.error.name).to.equal('ValidationError');
        done();
    });

    it('should return { error: "..." } when passed invalid recordType', (done) => {
        const question = {
            identifier: 'Question1',
            recordType: 'BadRecordType',
            questionText: {
                en: 'My question',
            },
        };
        const validatedQuestion = Joi.validate(question, Validator.questionSchema);
        expect(validatedQuestion.error).to.not.be.null();
        expect(validatedQuestion.error.name).to.equal('ValidationError');
        done();
    });
});

describe('study group schema', () => {
    it('should return {error: null, value: ...} when passed a valid study group', (done) => {
        const studyGroup = {
            identifier: 'studyGroupId',
            recordType: 'StudyGroup',
            lastModified: '1970-01-01T01:00:00Z',
            persistentIdentifier: 'doi:10.3886/ICPSR09212.v3',
            prefLabel: {
                en: 'Some label',
            },
            title: {
                en: 'Study group title',
            },
            subtitle: {
                en: 'Study group sub title',
            },
            abstract: {
                en: 'abstract',
            },
            alternative: {
                en: 'alternative',
            },
            available: '1970-01-01T01:00:00.000Z',
            purpose: {
                en: 'Some purpose',
            },
            fundedBy: [
                { en: 'organization1' },
                { en: 'organization2' },
            ],
            inGroup: [
                'groupId',
            ],
            kindOfData: {
                en: 'Kind of data',
            },
            universe: {
                en: 'Universe description for study',
            },
            ddiFile: [
                'DDIFile1',
                'DDIFile2',
            ],
        };
        const validatedStudyGroup = Joi.validate(studyGroup, Validator.studyGroupSchema);
        expect(validatedStudyGroup.error).to.equal(null);
        done();
    });

    it('should accept null and empty array/object for values other than identifier', (done) => {
        const studyGroup = {
            identifier: 'studyGroupId',
            recordType: 'StudyGroup',
            lastModified: '1970-01-01T01:00:00Z',
            persistentIdentifier: null,
            prefLabel: {},
            title: {},
            subtitle: {},
            abstract: {},
            alternative: {},
            available: null,
            purpose: {},
            fundedBy: [],
            inGroup: [],
            kindOfData: {},
            universe: {},
            ddiFile: [],
        };
        const validatedStudyGroup = Joi.validate(studyGroup, Validator.studyGroupSchema);
        expect(validatedStudyGroup.error).to.equal(null);
        done();
    });

    it('should return {error: "...", } when passed an invalid study group', (done) => {
        const studyGroup = {
            identifier: 'studyGroupId',
            recordType: 'StudyGroup',
            lastModified: '1970-01-01T01:00:00Z',
            persistentIdentifier: 'doi:10.3886/ICPSR09212.v3',
            prefLabel: {
                en: 'Some label',
            },
            ttile: { // here is the error, ttile and not 'title'
                en: 'Study group title',
            },
            subtitle: {
                en: 'Study group sub title',
            },
            abstract: {
                en: 'abstract',
            },
            alternative: {
                en: 'alternative',
            },
            available: '1970-01-01T01:00:00.000Z',
            purpose: {
                en: 'Some purpose',
            },
            fundedBy: [
                { en: 'organization1' },
                { en: 'organization2' },
            ],
            inGroup: [
                'groupId',
            ],
            kindOfData: {
                en: 'Kind of data',
            },
            universe: {
                en: 'Universe description for study',
            },
            ddiFile: [
                'DDIFile1',
                'DDIFile2',
            ],
        };
        const validatedStudyGroup = Joi.validate(studyGroup, Validator.studyGroupSchema);
        expect(validatedStudyGroup.error).to.not.be.null();
        expect(validatedStudyGroup.error.name).to.equal('ValidationError');
        done();
    });
});
