const Code = require('code');
const Lab = require('lab');
const nock = require('nock');
const RepoHandlerResolver = require('../../src/api/resolver');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const afterEach = lab.afterEach;
const expect = Code.expect;
const resolutionUrl = 'http://resolutionservice.example.com';
const ResolutionService = nock(resolutionUrl);

afterEach((done) => {
    nock.cleanAll();
    done();
});


describe('RepoHandlerResolver.resolveRepoHandlerUrl', () => {
    it('returns repo handler url when given resolver url and existing repository url', (done) => {
        ResolutionService
            .get('/v0/ResolveRepositoryURL')
            .matchHeader('X-Request-ID', 'so-unique')
            .query({ Repository: 'http://repository.example.com' })
            .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

        RepoHandlerResolver
            .resolveRepositoryUrl(resolutionUrl, 'http://repository.example.com', { 'x-request-id': 'so-unique' })
            .then((result) => {
                expect(result).to.equal('http://repositoryhandler.example.com');
                done();
            }).catch(done);
    });

    it('returns 404 not found when given non-existant repository url', (done) => {
        ResolutionService
            .get('/v0/ResolveRepositoryURL')
            .query({ Repository: 'http://nosuchrepo.example.com' })
            .reply(404, { message: 'Repository URL not found.' });

        RepoHandlerResolver
            .resolveRepositoryUrl(resolutionUrl, 'http://nosuchrepo.example.com', {})
            .then((result) => done(new Error(result)))
            .catch((error) => {
                expect(error.isBoom).to.be.true();
                expect(error.output.statusCode).to.equal(404);
                expect(error.output.payload.error).to.equal('Not Found');
                expect(error.output.payload.message).to.equal('Repository URL not found.');
                done();
            })
            .catch(done);
    });

    it('returns 500 error when resolution service returns 500', (done) => {
        ResolutionService
            .get('/v0/ResolveRepositoryURL')
            .query({ Repository: 'http://repository.example.com' })
            .reply(500, { message: 'Internal server error.' });

        RepoHandlerResolver
            .resolveRepositoryUrl(resolutionUrl, 'http://repository.example.com', {})
            .then((result) => done(new Error(result)))
            .catch((error) => {
                expect(error.isBoom).to.be.true();
                expect(error.output.statusCode).to.equal(500);
                expect(error.output.payload.error).to.equal('Internal Server Error');
                // Boom.badImplementation uses generic message to avoid revealing sensitive errors.
                expect(error.output.payload.message).to.equal('An internal server error occurred');
                done();
            })
            .catch(done);
    });
});
