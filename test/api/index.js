const Code = require('code');
const Lab = require('lab');
const nock = require('nock');
const cessdaMetadataHarvester = require('../../index');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const afterEach = lab.afterEach;
const expect = Code.expect;
const repositoryHandler = nock('http://repositoryhandler.example.com');
const resolutionService = nock('http://localhost:8081');


// We use server without starting it to provide a realistic testing ground
// that respects the server configuration. We need to nest the tests inside
// the callback.
cessdaMetadataHarvester((err, server) => {
    afterEach((done) => {
        nock.cleanAll();
        done();
    });

    const apiServer = server.select('api');
    describe('/', () => {
        it('redirects user to /documentation', (done) => {
            apiServer.inject('/', (res) => {
                expect(res.statusCode).to.equal(302);
                done();
            });
        });

        it('is given x-request-id when non provided by client', (done) => {
            apiServer.inject('/', (res) => {
                expect(res.request.headers['x-request-id']).to.exist();
                done();
            });
        });
    });

    describe('onPostAuth Handler', () => {
        it('should remove {param} style template values caused by swagger requests', (done) => {
            apiServer.inject('/v0/ListRecordHeaders/{RecordType}?Repository=http://repository.example.com', (res) => {
                expect(res.request.params).to.be.empty();
                done();
            });
        });

        it('should not remove valid params', (done) => {
            apiServer.inject('/v0/ListRecordHeaders/Study?Repository=http://repository.example.com', (res) => {
                expect(res.request.params).to.deep.equal({ RecordType: 'Study' });
                done();
            });
        });
    });

    describe('/v0', () => {
        describe('/*', () => {
            it('should pass along server caching headers to client', (done) => {
                const question = {
                    identifier: 'Question1',
                    recordType: 'Question',
                    lastModified: '1970-01-01T01:00:00Z',
                    questionText: { en: 'My question' },
                    inVariable: ['some_variable'],
                    representation: 'string',
                };

                repositoryHandler
                    .get('/v0/GetRecord/Question/Question1')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, question, {
                        'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT',
                        Etag: '686897696a7c876b7e',
                    });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/GetRecord/Question/Question1?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.headers['last-modified']).to.equal('Mon, 03 Jan 2011 17:45:57 GMT');
                    expect(res.headers.etag).to.equal('686897696a7c876b7e');
                    done();
                });
            });
        });
        describe('/ValidateRecord/Study', () => {
            it('returns 200 status when valid format', (done) => {
                const study = {
                    identifier: 'Some identifier',
                    recordType: 'Study',
                    lastModified: '1970-01-01T01:00:00Z',
                    prefLabel: {
                        en: 'Some label',
                        no: 'En merkelapp',
                    },
                    title: {
                        en: 'Some title',
                    },
                    subtitle: {
                        en: 'Some sub title',
                    },
                    alternative: {
                        en: 'Some alternative string',
                    },
                    abstract: {
                        en: 'Some abstract',
                    },
                    analysisUnit: {
                        en: 'Person',
                    },
                    ddiFile: [
                        'DDIFile1',
                        'DDIFile2',
                    ],
                    fundedBy: [
                        { en: 'organization1' },
                        { en: 'organization2' },
                    ],
                    inGroup: [
                        'studyGroupId1',
                        'studyGroupId2',
                    ],
                    instrument: [
                        'Instrument1',
                        'Instrument2',
                    ],
                    kindOfData: {
                        en: 'Kind of data',
                    },
                    subject: [
                        {
                            en: 'Subject 1',
                            no: 'Emne 1',
                        },
                        {
                            en: 'Subject 2',
                            no: 'Emne 2',
                        },
                    ],
                    product: 'LogicalDataset', // ID of logical dataset file
                    purpose: {
                        en: 'Some purpose',
                    },
                    universe: {
                        en: 'Universe description for study',
                    },
                    variable: [
                        'varid1',
                        'varid2',
                    ],
                    available: '1970-01-01T01:00:00.000Z',
                    temporal: {
                        startDate: '1970-01-01T01:00:00.000Z',
                        endDate: '1971-01-01T01:00:00.000Z',
                    },
                    spatial: [
                        {
                            en: 'Norway',
                        },
                        {
                            en: 'Sweden',
                        },
                    ],
                }; // End of study object

                const request = {
                    method: 'POST',
                    url: '/v0/ValidateRecord/Study',
                    payload: study,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                apiServer.inject(request, (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal({ message: 'Validation Success!' });
                    done();
                });
            });
        });

        describe('ValidateRecord/Variable', () => {
            it('should return 200 status when valid variable format', (done) => {
                const variable = {
                    identifier: 'Var1',
                    recordType: 'Variable',
                    lastModified: '1970-01-01T01:00:00Z',
                    prefLabel: {
                        en: 'Some label',
                        no: 'En merkelapp',
                    },
                    description: {
                        en: 'Some title',
                    },
                    subject: [{
                        en: 'Subject',
                    }],
                    notation: 'The variable name',
                    basedOn: 'Id of represented variable?',
                    analysisUnit: {
                        en: 'Person',
                    },
                    concept: {
                        en: 'Variable concept',
                    },
                    question: [
                        'questionid1',
                        'questionid2',
                    ],
                    inStudy: ['some_study'],
                    representation: 'string',
                    codeList: [
                        {
                            prefLabel: {
                                en: 'Male',
                                no: 'Mann',
                            },
                            notation: '1',
                        },
                        {
                            prefLabel: {
                                en: 'Female',
                            },
                            notation: '2',
                        },
                    ],
                    universe: {
                        en: 'Same description as study?',
                    },
                }; // End of variable

                const request = {
                    method: 'POST',
                    url: '/v0/ValidateRecord/Variable',
                    payload: variable,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };

                apiServer.inject(request, (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal({ message: 'Validation Success!' });
                    done();
                });
            });
        });

        describe('ValidateRecord/Question', () => {
            it('should return 200 status when valid question format', (done) => {
                const question = {
                    identifier: 'Question1',
                    recordType: 'Question',
                    lastModified: '1970-01-01T01:00:00Z',
                    questionText: {
                        en: 'My question',
                    },
                    inVariable: ['some_variable'],
                    representation: 'string',
                    codeList: [
                        {
                            prefLabel: {
                                en: 'Male',
                                no: 'Mann',
                            },
                            notation: '1',
                        },
                        {
                            prefLabel: {
                                en: 'Female',
                            },
                            notation: '2',
                        },
                    ],
                }; // End of variable

                const request = {
                    method: 'POST',
                    url: '/v0/ValidateRecord/Question',
                    payload: question,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };

                apiServer.inject(request, (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal({ message: 'Validation Success!' });
                    done();
                });
            });
        });

        describe('ValidateRecord/StudyGroup', () => {
            it('should return 200 status when valid studyGroup format', (done) => {
                const studyGroup = {
                    identifier: 'studyGroupId',
                    recordType: 'StudyGroup',
                    lastModified: '1970-01-01T01:00:00Z',
                    prefLabel: {
                        en: 'Some label',
                    },
                    title: {
                        en: 'Study group title',
                    },
                    subtitle: {
                        en: 'Study group sub title',
                    },
                    abstract: {
                        en: 'abstract',
                    },
                    alternative: {
                        en: 'alternative',
                    },
                    available: '1970-01-01T01:00:00.000Z',
                    purpose: {
                        en: 'Some purpose',
                    },
                    fundedBy: [
                        { en: 'organization1' },
                        { en: 'organization2' },
                    ],
                    inGroup: [
                        'groupId',
                    ],
                    kindOfData: {
                        en: 'Kind of data',
                    },
                    universe: {
                        en: 'Universe description for study',
                    },
                    ddiFile: [
                        'DDIFile1',
                        'DDIFile2',
                    ],
                }; // End of variable

                const request = {
                    method: 'POST',
                    url: '/v0/ValidateRecord/StudyGroup',
                    payload: studyGroup,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };

                apiServer.inject(request, (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal({ message: 'Validation Success!' });
                    done();
                });
            });
        });

        describe('/ListRecordHeaders', () => {
            it('should return a json array of record header properties if valid repo', (done) => {
                const recordHeaders = [
                    {
                        identifier: 'NSD42',
                        recordType: 'RecordHeader',
                        type: 'Study',
                        lastModified: '2006-01-01T01:00:00.000Z',
                    },
                    {
                        identifier: 'NSD42',
                        recordType: 'RecordHeader',
                        type: 'Variable',
                        lastModified: '2006-01-01T01:00:00.000Z',
                    },
                ];

                repositoryHandler
                    .get('/v0/ListRecordHeaders')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, recordHeaders, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/ListRecordHeaders?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result[0]).to.only.include(['identifier', 'lastModified', 'type', 'recordType']);
                    expect(res.result).to.deep.equal(recordHeaders);
                    done();
                });
            });

            it('should return one type of record header when type is specified', (done) => {
                const studyHeaders = [
                    {
                        identifier: 'NSD42',
                        recordType: 'RecordHeader',
                        type: 'Study',
                        lastModified: '2006-01-01T01:00:00.000Z',
                    },
                    {
                        identifier: 'NSD42',
                        recordType: 'RecordHeader',
                        type: 'Study',
                        lastModified: '2006-01-01T01:00:00.000Z',
                    },
                ];

                repositoryHandler
                    .get('/v0/ListRecordHeaders/Study')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, studyHeaders, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/ListRecordHeaders/Study?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result[0]).to.only.include(['identifier', 'lastModified', 'type', 'recordType']);
                    expect(res.result).to.deep.equal(studyHeaders);
                    done();
                });
            });

            it('should return 500 if repo handler server error', (done) => {
                repositoryHandler
                    .get('/v0/ListRecordHeaders')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(500, { message: 'Internal Handler Server Error' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/ListRecordHeaders?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(500);
                    expect(res.result.error).to.equal('Internal Server Error');
                    done();
                });
            });

            it('should return 500 if resolution service internal server error', (done) => {
                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(500, { message: 'Internal Resolution Server Error' });

                apiServer.inject('/v0/ListRecordHeaders?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(500);
                    expect(res.result.error).to.equal('Internal Server Error');
                    done();
                });
            });
        });

        describe('/GetRecord/Study', () => {
            it('should return study from repo handler when study valid', (done) => {
                const study = {
                    identifier: 'NSD1234',
                    recordType: 'Study',
                    lastModified: '1970-01-01T01:00:00Z',
                    prefLabel: { en: 'Some label' },
                    title: { en: 'Some title' },
                    subtitle: { en: 'Some sub title' },
                    alternative: { en: 'Some alternative string' },
                    abstract: { en: 'Some abstract' },
                    analysisUnit: { en: 'Person' },
                    ddiFile: ['DDIFile1'],
                    fundedBy: [{ en: 'organization1' }],
                    inGroup: ['studyGroupId1'],
                    instrument: ['Instrument1'],
                    kindOfData: { en: 'Kind of data' },
                    subject: [{ en: 'Subject 1' }],
                    product: 'LogicalDataset', // ID of logical dataset file
                    purpose: { en: 'Some purpose' },
                    universe: { en: 'Universe description for study' },
                    variable: ['varid1'],
                    available: '1970-01-01T01:00:00.000Z',
                    temporal: {
                        startDate: '1970-01-01T01:00:00.000Z',
                        endDate: '1971-01-01T01:00:00.000Z',
                    },
                    spatial: [{ en: 'Norway' }],
                };

                repositoryHandler
                    .get('/v0/GetRecord/Study/NSD1234')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, study, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/GetRecord/Study/NSD1234?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal(study);
                    done();
                });
            });
        });

        describe('/GetRecord/StudyGroup', () => {
            it('should return study group from repo handler when study group valid', (done) => {
                const studyGroup = {
                    identifier: 'StudyGroup1',
                    recordType: 'StudyGroup',
                    lastModified: '1970-01-01T01:00:00Z',
                    prefLabel: { en: 'Some label' },
                    title: { en: 'Study group title' },
                    subtitle: { en: 'Study group sub title' },
                    abstract: { en: 'abstract' },
                    alternative: { en: 'alternative' },
                    available: '1970-01-01T01:00:00.000Z',
                    purpose: { en: 'Some purpose' },
                    fundedBy: [
                        { en: 'organization1' },
                        { en: 'organization2' },
                    ],
                    inGroup: ['groupId'],
                    kindOfData: { en: 'Kind of data' },
                    universe: { en: 'Universe description for study' },
                    ddiFile: ['DDIFile1'],
                };

                repositoryHandler
                    .get('/v0/GetRecord/StudyGroup/StudyGroup1')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, studyGroup, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                const url = '/v0/GetRecord/StudyGroup/StudyGroup1?Repository=http://repository.example.com';
                apiServer.inject(url, (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal(studyGroup);
                    done();
                });
            });
        });

        describe('/GetRecord/Variable', () => {
            it('should return variable from repo handler when variable is valid', (done) => {
                const variable = {
                    identifier: 'Var1',
                    recordType: 'Variable',
                    lastModified: '1970-01-01T01:00:00Z',
                    prefLabel: { en: 'Some label' },
                    description: { en: 'Some title' },
                    subject: [{
                        en: 'Subject',
                    }],
                    notation: 'The variable name',
                    basedOn: 'Id of represented variable?',
                    analysisUnit: { en: 'Person' },
                    concept: { en: 'Variable concept' },
                    question: ['questionid1'],
                    inStudy: ['some_study'],
                    representation: 'string',
                    codeList: [
                        {
                            prefLabel: {
                                en: 'Male',
                                no: 'Mann',
                            },
                            notation: '1',
                        },
                    ],
                    universe: { en: 'Same description as study?' },
                }; // End of variable

                repositoryHandler
                    .get('/v0/GetRecord/Variable/Var1')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, variable, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/GetRecord/Variable/Var1?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal(variable);
                    done();
                });
            });
        });

        describe('/GetRecord/Question', () => {
            it('should return question from repo handler when question is valid', (done) => {
                const question = {
                    identifier: 'Question1',
                    recordType: 'Question',
                    questionText: { en: 'My question' },
                    inVariable: ['some_variable'],
                    representation: 'string',
                    lastModified: '1970-01-01T01:00:00Z',
                    codeList: [
                        {
                            prefLabel: {
                                en: 'Male',
                                no: 'Mann',
                            },
                            notation: '1',
                        },
                    ],
                };

                repositoryHandler
                    .get('/v0/GetRecord/Question/Question1')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, question, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/GetRecord/Question/Question1?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal(question);
                    done();
                });
            });
        });

        describe('/ListSupportedRecordTypes', () => {
            it('should return supported record types', (done) => {
                const supportedRecordTypes = ['Study', 'Question', 'StudyGroup', 'Variable'];
                repositoryHandler
                    .get('/v0/ListSupportedRecordTypes')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, supportedRecordTypes, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/ListSupportedRecordTypes?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(200);
                    expect(res.result).to.deep.equal(supportedRecordTypes);
                    done();
                });
            });

            it('should detect unknown record types', (done) => {
                const unknownTypes = ['MetaStudy'];
                repositoryHandler
                    .get('/v0/ListSupportedRecordTypes')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, unknownTypes, { 'Last-Modified': 'Mon, 03 Jan 2011 17:45:57 GMT' });

                resolutionService
                    .get('/v0/ResolveRepositoryURL')
                    .query({ Repository: 'http://repository.example.com' })
                    .reply(200, { repositoryHandlerUrl: 'http://repositoryhandler.example.com' });

                apiServer.inject('/v0/ListSupportedRecordTypes?Repository=http://repository.example.com', (res) => {
                    expect(res.statusCode).to.equal(500);
                    done();
                });
            });
        });
    });
});
