const Code = require('code');
const Lab = require('lab');
const cessdaMetadataHarvester = require('../index');

const lab = exports.lab = Lab.script();
const describe = lab.describe;
const it = lab.it;
const expect = Code.expect;

describe('cessda-metadata-harvester', () => {
    it('Server can be started', (done) => {
        cessdaMetadataHarvester((err, server) => {
            const apiServer = server.select('api');
            expect(err).to.not.exist();
            expect(apiServer).to.exist();
            expect(apiServer.info.host).to.equal('localhost');
            expect(apiServer.info.port).to.equal(8080);
            done();
        });
    });
});
