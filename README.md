# Metadata Harvester - **COPY FOR USE AT CES2016**

## About

This project implements the OSMH, Open Source Metadata Harvester. Its
background is described in the OSHM system architecture document:

> The OSMH is designed to support metadata harvesting from heterogeneous and
autonomous Metadata Repositories across Europe for the purpose of bringing
metadata holdings into centralised discovery systems (e.g. the CESSDA Products
and Services Catalog).

The project implements a REST API exposing metadata on the study group, study,
variable and question levels. The project is implemented using the server-side
[Node.js](https://nodejs.org/en/) Javascript runtime and the
[hapi](http://hapijs.com/) web framework.


### Why Node.js, JavaScript (ES6), Hapi, and Joi?

There are a couple of good reasons for chosing the technology stack used in
this project.

The OSMH is a data proxy for services using JSON as a transport format.
[Node.js](https://nodejs.org/en/) is a runtime which lets you write Javascript
server applications, and Javascript supports JSON data rather nicely. Most of
the operations in the OSMH is long-running HTTP requests, so most of the
execution time per request is spent waiting for data. Node.js is non-blocking
running all requests on the same thread in a fully asynchronous manner.
This means it does not waste threads (and thus memory) on waiting for HTTP
responses and can utilize its one thread very efficiently by switching
execution between requests when one request makes an HTTP call to a
repository handler. In theory the metadata harvester should be able to handle
more load, even with the validations requiring some computation.

Ultimately Javascript is also a common denominator programming language in the
development world. Because the OSMH is an open-source project, and might be
picked up by other developers in the future, it is important that the code be
easily understood.

[Hapi](http://hapijs.com/) is a web framework developed by Walmart Labs.
It favours configuration objects over code, and makes it dead simple to write
good HTTP apis with endpoints and HTTP actions (GET, POST, and so forth). More
importantly, the configuration approach means it's easy to read and understand
the application for outside developers.

[Joi](https://github.com/hapijs/joi) is a schema based JSON validator with
origins in the Hapi ecosystem. It seems to serve some or most of the same
purpose as JSON Schema. Joi seemed like a good choice as it seamlessly
integrates with Hapi providing validation of both request input and response
ouput. Joi schemes are also used by the Hapi-Swagger plugin to automtically
generate request and response models for the API documentation.


### Repository Handler Resolution Service

The OSMH Architecture document outlines a Repository Handler Resolution Service
which job it is to resolve (e.g. translate) repository URLs to Repository
Handler URLs. There are currently no accurate specification for this service.
In liue of a dedicated resolution service a resolution service is integrated
into this project, the OSMH.

The service is implemented as part of the same application (i.e. Node.js
process), but will listen on an individual port. It is implemented to act as
its own HTTP server so it can most easily be split out into a separate service
later on.

To properly understand the Cessda Harvester API flow see the following sequence
diagram:

![Harvester API flow](./docs/osmhflow.png)

## License

[Apache-2.0 Copyright CESSDA AS 2016](https://bitbucket.org/cessda/cessda.metadata.harvester/wiki/License)

## Contributor's Guide

Before contributing/comitting to this project please do the following.

* Read through the [CESSDA Developer Guidelines](https://bitbucket.org/cessda/cessda.guidelines.cit/wiki/Developers).
* Be sure that you have signed the CESSDA contributor license before contributing code. You only have to do this once for all CESSDA code repositories on Bitbucket.

Submit any technical bugs in the
[Issue tracker](https://bitbucket.org/cessda/cessda.metadata.harvester/issues?status=new&status=open).
Fork the project if you want to make contributions. Read more in
[How to make a contribution](https://bitbucket.org/cessda/cessda.guidelines.cit/wiki/How%20to%20make%20a%20contribution) guide.

## Install

You need Node.js >= 5.0.0 and npm to run the Metadata Harvester. You can
install Node and npm from [Node.js](https://nodejs.org/en/), homebrew
(`brew install node`), and
[NodeSource's official binary distributions](https://github.com/nodesource/distributions).
The official repositories of Debian, Ubuntu, and other Linux distributions might
not provide newer versions than Node 4.

Check that node is installed and install dependencies by running the below
commands from project root. Note the `--production` flag on `npm install`. This
flag tells npm not to install development dependencies. If you are installing
the dependencies for development, drop the `--production` flag.

```bash
$ node --version
v5.7.1

$ npm --version
3.6.0

$ npm install --production
open-source-metadata-harvester@0.0.0 /Users/snorre/workspaces/cessda.metadata.harvester
├─┬ glue@3.2.0
│ ├── items@2.0.0
│ └── joi@7.3.0
...
```

If everything installs correctly your output should look something similar to
the otput above. `npm install` installs the project dependencies, both for
running the application, and the development dependencies, such as testing
libraries.

Note the `npm-shrinkwrap.json` file in the repository. This file specifies the
entire recursive dependency graph of the project with exact version numbers.
This is done to prevent the same project version from resulting in different
dependencies which somtimes breaks an application when package authors
mishandle their versioning. npm will install any aditional dependencies from
the `package.json` dependency list.

Freezing the package versions is also a good way to mitigate the chance of
automatically installing a new malicious release of a package should a
package author loose control of their npm credentials. It does not completely
remove the risk.

Contributors should take care to run `npm shrinkwrap` whenever installing a new
package or updating packages. Also run `npm outdated` to see if there are any packages
that needs updating.

### Docker

The repository contains a `Dockerfile` that can be used to create a Docker
image of the OSMH. [Docker](https://www.docker.com/) is a container
library that enables you to build, run, and supervise containers. Applications
running in a Docker container runs isolated from other processes, and often
do not have direct access to the host's volumes (file system) and network.

To build the Open Source Metadata Harvester Docker image you can run:

```bash
$ docker build -t cessda/open-source-metadata-harvester:latest .
```

This will tell the Docker engine to build a Docker image based on the
Dockerfile in the current directory `.`, with the group `cessda` and
name `open-source-metadata-harvester` and tag the image name with
`latest`. `latest` is a community practice that allows you to always
depend on the latest version of a Docker image. You can also specify a
version:

```bash
$ docker build -t cessda/open-source-metadata-harvester:0.0.1 .
```

## User Guide

### Run

After finishing the installation you can run the project with the npm command:

```bash
$ npm start

> open-source-metadata-harvester@0.0.0 start /Users/snorre/workspaces/cessda.metadata.harvester
> node server.js

160405/225741.994, [log], data: ✅  Server is listening on http://localhost:8080
160405/225742.003, [log], data: ✅  Server is listening on http://localhost:8081
```

This will by default start a server on [localhost:8080](http://localhost:8080).
The resolution service is started at [localhost:8080](http://localhost:8081).

`npm start` is an alias for `node server.js` which runs the server as a
foreground process. You may optionally specify environment variables to
configure the host and port the OSMH and resolution servers should listen to.

```bash
$ OSMH_HOST=0.0.0.0 OSMH_PORT=9999 npm start

> open-source-metadata-harvester@0.0.0 start /Users/snorre/workspaces/cessda.metadata.harvester
> node server.js

160405/225741.994, [log], data: ✅  Server is listening on http://0.0.0.0:9999
160405/225742.003, [log], data: ✅  Server is listening on http://localhost:8081
```

You may optionally supply the environment variables in a .env file if you
prefer configuration files. If so, copy the `.env.dist` file to `.env` and
specify your configuration of choice within. For example:

```sh
OSMH_HOST=0.0.0.0
OSMH_PORT=9999
RESOLUTION_SERVICE_HOST=0.0.0.0
RESOLUTION_SERVICE_PORT=10000
```

If you need to run the server as a daemon/background
process/service you can use your OS of choice own process manager. For Linux
you can for instance set up a System.d init script.

Refer to [Stack Overflow](http://stackoverflow.com/questions/4018154/node-js-as-a-background-service/17005935#17005935)
for options.

#### Remote access to API examples in Swagger
 
If you are running the metadata harvester and want to share the API documentation and clickable Swagger examples
with people on a different host, configure `.env` or `npm start` like the following:

```sh
SWAGGER_SCHEME=http or https (http is default)
SWAGGER_HOST=my_reachable_host
SWAGGER_PORT=my_reachable_port
```

#### Specifying a repository mapping file for Resolution Service

The resolution service uses a JSON-file to map repository URLs to repository
handler URLs. This mapping file's format is super simple and looks like this:

```json
{
    "http://my-repository.example.com": "http://my-repository-handler.example.com",
    "http://my-other-repository.example.com": "http://my-repository-handler.example.com"
}
```

Note that the properties are the full repository URL without any trailing
slashes. The repository handler is similarily a full URL. The repository URLs
should be unique as a JSON object requires unique keys. The same repository
handler can of course be used multiple times.

To make the Resolution Handler server use your mapping file you must supply an
environment variable when running the application. If you have a mapping file
at `/home/myuser/cessda/mappingfile.json` you should run the application like so:

```bash
$ RESOLUTION_SERVICE_MAPPINGS=/home/myuser/cessda/mappingfile.json npm start --production
```

The mappings file is dynamically loaded for each request received by the
resolution service. In effect you should be able to add new repositories
to the file without restarting the resolution service.

Note that you do not need to configure the host and port of the resolution
service. By default the resolution service will bind to `localhost:8081` and
the harvester will make any `/ResolveRepositoryURL` requests to that address.
When running the harvester the resolution service will run on the same host,
so the harvester can safely connect to the resolution service over localhost.
If you do need to change the port of the resolution service use the
`RESOLUTION_SERVICE_PORT` environment variable. This will make the resolution
service listen to the given port as well as make the harvester use this new
port to perform the `/ResolveRepositoryURL` request.

#### Running with Docker

If you have created a Docker image as described in the installation section
above you may run the application inside a Docker container by issuing a
command like so:

```bash
$ docker run -p 127.0.0.1:8080:8080 -e OSMH_HOST=0.0.0.0 -it cessda/cessda-metadata-harvester:latest
```

This will start a Docker container based on the OSMH Docker image. The Docker
daemon will map port `127.0.0.1:8080` on your host's network to the Docker
container's port `0.0.0.0:8080` on the container's network. To access the
OSMH application inside the container we need to make it listen to all
requests, not just localhost (inside the container). To achieve this we set the
environment variable with `-e OSMH_HOST=0.0.0.0`.

Note that because the resolution service runs in the same container as the OSMH
we can allow the OSMH to communicate with it with URL `127.0.0.1:8081`. If you
want to specify a custom mapping file you need to map this file into the
container. Read more about [mapping volumes here](https://docs.docker.com/engine/userguide/containers/dockervolumes/). 

### How to use API

After running the Node application the api documentation should be available at
[localhost:8080](http://localhost:8080). The server uses Swagger to render the
API specification and provides functionality to make requests against the API.

[API-documentation](./docs/API.md)

![Swagger API Docs](./docs/apidocs.png)

## Development

Preferably, especially for non maintainers, create a fork of the project. Then
clone project files:

```bash
## If working on main repository
git clone git@bitbucket.org:cessda/cessda.metadata.harvester.git

## If working on fork
git clone git@bitbucket.org:<username>/cessda.metadata.harvester.git
```

Follow node and npm installation instructions under install. Then run npm
install without `--production` flag.

```bash
$ node --version
v5.7.1

$ npm --version
3.6.0

$ npm install
open-source-metadata-harvester@0.0.0 /Users/snorre/workspaces/cessda.metadata.harvester
├─┬ glue@3.2.0
│ ├── items@2.0.0
│ └── joi@7.3.0
...
```
### Run development mode

When you are developing you may want to run the server so it restarts the
server and reruns the tests whenever code changes. This enables rapid feedback.
The `project.json` file comes with some scripts to do exactly this.

Running server with auto-reload:
```bash
$ npm run watch-source

> open-source-metadata-harvester@0.0.0 watch-source /Users/snorre/workspaces/cessda.metadata.harvester
> nodemon server.js

[nodemon] 1.9.1
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: *.*
[nodemon] starting `node server.js`
160405/225741.994, [log], data: ✅  Server is listening on http://localhost:8080
160405/225742.003, [log], data: ✅  Server is listening on http://localhost:8081
```


### Testing

This project uses the [lab](https://www.npmjs.com/package/lab) test utility and
[code](https://www.npmjs.com/package/code) assertion library to do testing. You
can run all the project tests once with `npm test`. This will run `lab ./test
-c` which runs all tests contained in the testing directory and outputs a code
coverage percentage as well as returns an exit code to the shell. This makes it
easy to run tests.

Re-running tests on changes:
```bash
$ npm run watch-test
> open-source-metadata-harvester@0.0.0 watch-test /Users/snorre/workspaces/cessda.metadata.harvester
> nodemon --watch ./test --watch ./src --watch ./index.js --exec 'npm run test'

[nodemon] 1.9.1
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: /Users/snorre/workspaces/cessda.metadata.harvester/test/**/* /Users/snorre/workspaces/cessda.metadata.harvester/src/**/* ./index.js
[nodemon] starting `npm run test`

> open-source-metadata-harvester@0.0.0 test /Users/snorre/workspaces/cessda.metadata.harvester
> lab -c ./test

5 tests complete
Test duration: 73 ms
No global variable leaks detected
Coverage: 98.47% (2/131)
index.js missing coverage on line(s): 15, 16

[nodemon] clean exit - waiting for changes before restart
```

### Linting

This project uses the [ESLint](http://eslint.org/) Javascript linter to check
for bad code styles and patterns. The harvester project uses [airbnb's eslint config](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb)
for its default ESLint config, but overrides the indent spacing to require four
spaces instead of two.

you may want to run ESLint inside your editor. Most editors should have an
ESLint plugin for this.

You can run the linter on the command line with:

```bash
$ npm run lint

> open-source-metadata-harvester@0.0.0 lint /home/smd/workspaces/cessda/cessda-metadata-harvester
> eslint ./
```

The `package.json` file also specifies a command for running the linting continuously:

```bash
$ npm run watch-lint

> open-source-metadata-harvester@0.0.0 watch-lint /home/smd/workspaces/cessda/cessda-metadata-harvester
> nodemon --watch ./test --watch ./src --watch ./index.js --exec 'npm run lint'

[nodemon] 1.9.1
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: /home/smd/workspaces/cessda/cessda-metadata-harvester/test/**/* /home/smd/workspaces/cessda/cessda-metadata-harvester/src/**/* ./index.js
[nodemon] starting `npm run lint`

> open-source-metadata-harvester@0.0.0 lint /home/smd/workspaces/cessda/cessda-metadata-harvester
> eslint ./

```


## Logging 

Errors (responses with http status code >= 400) are logged to the console as structured JSON. See example below:

```
{
	“time”: “2012-03-28T17:26:38.431Z”,
	“host”: “abc.cessda.lan”,
	“port”: 80,
	“status”: 200,
	“level”: “info”,
	“message” : “start”
	“responseTime”: 17,
	“request”: {
		“method”: “GET”,
		“url”: “/v1.1/ListRecordHeaders?repository=http://r.archive.net”,
		“headers”: {
			“user-agent”: “curl/7.19.7”,
			“X-REQUEST-ID”, “ed505307-bc44...”,
			“Accept-Language”, “da, en-gb; q0.8”,
			“ETag”: "6d82cbb050ddc7fa9cbb659014546e59"

				}
			}
}
```