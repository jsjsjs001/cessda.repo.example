'use strict';

const internals = {};

const Through = require('through2');
const stringify = require('json-stringify-safe');
const host = require('os').hostname();
const Squeeze = require('good-squeeze').Squeeze;
let port = undefined;

module.exports = internals.JsonConsole = function constructor(events, options) {
    if (!(this instanceof internals.JsonConsole)) {
        return new internals.JsonConsole(events, options);
    }
    this._options = options;
    this._filter = new Squeeze(events);
    return this;
};

internals.JsonConsole.prototype.init = function init(stream, emitter, callback) {
    const self = this;
    port = emitter._server.select('api').info.port;

    if (!stream._readableState.objectMode) {
        return callback(new Error('stream must be in object mode'));
    }

    stream.pipe(this._filter).pipe(Through.obj(function transform(data, enc, next) {
        const eventName = data.event;
        const log = (json) => {
            this.push(`${stringify(json, null, 0)}\n`);
        };
        if (eventName === 'response' && data.statusCode < 400) {
            // drop 200, 304, etc (lower than 400) responses from logging
        } else if (eventName === 'response') {
            log(self._responseEvent(data));
        } else if (eventName === 'log') {
            log(self._logEvent(data));
        } else if (eventName === 'request') {
            log(self._requestEvent(data));
        } else {
            log(self._otherEvent(data));
        }
        return next();
    })).pipe(process.stdout);

    return callback();
};

internals.JsonConsole.prototype.statusCodeToLevel = function statusCodeToLevel(statusCode) {
    if (statusCode >= 500 && statusCode < 600) {
        return 'error';
    } else if (statusCode >= 400 && statusCode < 500) {
        return 'warning';
    }
    return 'info';
};

internals.JsonConsole.prototype._responseEvent = function _formatResponseEvent(data) {
    return {
        time: new Date(data.timestamp).toISOString(),
        host,
        port,
        status: data.statusCode,
        level: this.statusCodeToLevel(data.statusCode),
        message: 'response',
        responseTime: data.responseTime,
        request: {
            method: data.method.toUpperCase(),
            url: data.path,
            params: data.query,
            headers: data.headers,
        },
        source: data.source,
        responsePayload: data.responsePayload,
    };
};

internals.JsonConsole.prototype._logEvent = function _formatLogEvent(data) {
    return {
        time: new Date(data.timestamp).toISOString(),
        host,
        port,
        level: 'info',
        message: 'log',
        data: data.data,
    };
};

internals.JsonConsole.prototype._requestEvent = function _requestEvent(data) {
    data.data.time = data.data.time || new Date(data.timestamp).toISOString(); // eslint-disable-line no-param-reassign
    data.data.host = data.data.host || host; // eslint-disable-line no-param-reassign
    data.data.port = data.data.port || port; // eslint-disable-line no-param-reassign
    return data.data;
};

internals.JsonConsole.prototype._otherEvent = function _formatOtherEvent(data) {
    return {
        time: new Date(data.timestamp).toISOString(),
        host,
        port,
        level: 'info',
        message: 'unhandled event',
        raw: data,
    };
};
