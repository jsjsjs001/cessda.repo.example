const uuid = require('node-uuid'); // Implementation of RFC4122 (v1 and v4) UUIDs.

exports.register = (server, options, next) => {
    server.ext('onRequest', (request, reply) => {
        const headers = request.headers;
        const hasXRequestId = Object.keys(headers).some((key) =>
            key.toLowerCase() === 'x-request-id');

        if (!hasXRequestId) headers['x-request-id'] = uuid.v4();

        return reply.continue();
    });

    server.ext('onPreResponse', (request, reply) => {
        const requestId = request.headers['x-request-id'];
        if (request.response.header) {
            request.response.header('X-Request-ID', requestId);
        } else if (request.response.output && request.response.output.headers) {
            const headers = request.response.output.headers;
            headers['X-Request-ID'] = requestId;
        }
        return reply.continue();
    });
    next();
};

const projectPkg = require('../../package.json');
const pkg = {
    name: 'x-request-id',
    version: projectPkg.version,
};

exports.register.attributes = {
    pkg,
};
