'use strict';

const Joi = require('joi');
const Validator = require('./validator');
const Resolver = require('./resolver');
const Client = require('./repository-handler-client');

const internals = {
    // Validation for 200 responses
    serverResponse: Joi.object({
        message: Joi.string().required(),
    }).unknown(true),
    // Validation for 304 Not Modified responses
    notModified: Joi.object({}),
    // Validation for 400 validation errors
    validationError: Joi.object({
        statusCode: Joi.number().valid(400).required(),
        error: Joi.string().valid('Bad Request').required(),
        message: Joi.string().required(),
        validation: Joi.object({
            source: Joi.string().required(),
            keys: Joi.array().items(Joi.string()).required(),
        }),
    }).description('Validation failed'),
    // Validation for 500 internal server errors
    internalServerError: Joi.object({
        message: Joi.string(),
        error: Joi.string(),
        statusCode: Joi.number(),
    }).unknown(true).description('Unexpected internal server error'),
    notFound: Joi.object({
        message: Joi.string(),
        error: Joi.string(),
        statusCode: Joi.number().valid(404),
    }).unknown(true).description('Not found'),
    cachingAndTracingHeaders: Joi.object({
        'If-Modified-Since': Joi.string(),
        'If-None-Match': Joi.string(),
        'X-Request-ID': Joi.string(),
    }).options({ allowUnknown: true }),
};

/**
 * Remove swagger-generated template placeholders on the form {param}.
 *
 * When Hapi specifies an optional path parameter hapi-swagger will
 * insert a placeholder {param-name} value in the URL in place of a
 * proper parameter value instead of omitting the parameter. To avoid
 * getting errors when testing the API through Swagger we remove such
 * parameter placeholders.
 *
 * @param {Request} request - the Hapi request object
 * @param {IReply} reply - the Hapi reply object
 */
internals.removeTemplateParamValues = (request, reply) => {
    const params = request.params;
    for (const param in params) {
        if (params.hasOwnProperty(param)) {
            const paramValue = request.params[param];
            // Match all params surrounded by {} where value == param key
            if (paramValue.match(new RegExp(`\{${param}\}`))) {
                delete params[param];
            }
        }
    }
    return reply.continue();
};

internals.loggerForRequest = (request) =>
    (data) => {
        const requestId = data['x-request-id'] || request.headers['x-request-id'];
        data['x-request-id'] = requestId; // eslint-disable-line no-param-reassign
        data.level = data.level || 'info'; // eslint-disable-line no-param-reassign
        request.log([], data);
    };

/**
 * Apply caching headers from repository handler response to Hapi reply.
 *
 * @param {IReply} reply - The hapi reply object
 * @param {Object} headers - The json object containing repo handler reply headers
 */
internals.setCacheHeaders = (reply, headers) => {
    const headerList = ['Last-Modified', 'ETag', 'Cache-Control'];

    headerList.forEach((header) => {
        const headerValue = headers[header] || headers[header.toLowerCase()];
        if (headerValue !== undefined && headerValue !== null) {
            reply.header(header, headerValue);
        }
    });

    return reply;
};

exports.register = function register(plugin, options, next) {
    plugin.ext('onPostAuth', internals.removeTemplateParamValues);

    plugin.route([
        {
            method: 'GET',
            path: '/',
            handler: (request, reply) => reply.redirect('/documentation'),
        },
        {
            method: 'GET',
            path: '/v0/GetRecord/Question/{Identifier}',
            config: {
                tags: ['api'],
                description: 'Returns the question record',
                validate: {
                    headers: internals.cachingAndTracingHeaders,
                    params: {
                        Identifier: Joi.string().required(),
                    },
                    query: {
                        Repository: Joi.string().required(),
                    },
                },
                response: {
                    status: {
                        200: Validator.questionSchema,
                        304: internals.notModified,
                        400: internals.validationError,
                        404: internals.notFound,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => {
                const repository = request.query.Repository;
                const identifier = request.params.Identifier;
                const logger = internals.loggerForRequest(request);

                Resolver.resolveRepositoryUrl(options.resolutionServiceUrl, repository, request.headers)
                    .then((url) => Client.getQuestionV0(url, repository, request.headers, identifier, logger))
                    .then((result) => {
                        internals.setCacheHeaders(reply(result.payload), result.headers)
                            .code(result.statusCode);
                    })
                    .catch((boomError) => reply(boomError));
            },
        },
        {
            method: 'GET',
            path: '/v0/GetRecord/Study/{Identifier}',
            config: {
                tags: ['api'],
                description: 'Returns the study record',
                validate: {
                    headers: internals.cachingAndTracingHeaders,
                    params: {
                        Identifier: Joi.string().required(),
                    },
                    query: {
                        Repository: Joi.string().required(),
                    },
                },
                response: {
                    status: {
                        200: Validator.studySchema,
                        304: internals.notModified,
                        400: internals.validationError,
                        404: internals.notFound,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => {
                const repository = request.query.Repository;
                const identifier = request.params.Identifier;
                const logger = internals.loggerForRequest(request);

                Resolver.resolveRepositoryUrl(options.resolutionServiceUrl, repository, request.headers)
                    .then((url) => Client.getStudyV0(url, repository, request.headers, identifier, logger))
                    .then((result) => {
                        internals.setCacheHeaders(
                            reply(result.payload), result.headers).code(result.statusCode);
                    })
                    .catch((boomError) => reply(boomError));
            },
        },
        {
            method: 'GET',
            path: '/v0/GetRecord/StudyGroup/{Identifier}',
            config: {
                tags: ['api'],
                description: 'Returns the study group record',
                validate: {
                    headers: internals.cachingAndTracingHeaders,
                    params: {
                        Identifier: Joi.string().required(),
                    },
                    query: {
                        Repository: Joi.string().required(),
                    },
                },
                response: {
                    status: {
                        200: Validator.studyGroupSchema,
                        304: internals.notModified,
                        400: internals.validationError,
                        404: internals.notFound,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => {
                const repository = request.query.Repository;
                const identifier = request.params.Identifier;
                const logger = internals.loggerForRequest(request);

                Resolver.resolveRepositoryUrl(options.resolutionServiceUrl, repository, request.headers)
                    .then((url) => Client.getStudyGroupV0(url, repository, request.headers, identifier, logger))
                    .then((result) => {
                        internals.setCacheHeaders(reply(result.payload), result.headers)
                            .code(result.statusCode);
                    })
                    .catch((boomError) => reply(boomError));
            },
        },
        {
            method: 'GET',
            path: '/v0/GetRecord/Variable/{Identifier}',
            config: {
                tags: ['api'],
                description: 'Returns the variable record',
                validate: {
                    headers: internals.cachingAndTracingHeaders,
                    params: {
                        Identifier: Joi.string().required(),
                    },
                    query: {
                        Repository: Joi.string().required(),
                    },
                },
                response: {
                    status: {
                        200: Validator.variableSchema,
                        304: internals.notModified,
                        400: internals.validationError,
                        404: internals.notFound,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => {
                const repository = request.query.Repository;
                const identifier = request.params.Identifier;
                const logger = internals.loggerForRequest(request);

                Resolver.resolveRepositoryUrl(options.resolutionServiceUrl, repository, request.headers)
                    .then((url) => Client.getVariableV0(url, repository, request.headers, identifier, logger))
                    .then((result) => {
                        internals.setCacheHeaders(reply(result.payload), result.headers)
                            .code(result.statusCode);
                    })
                    .catch((boomError) => reply(boomError));
            },
        },
        {
            method: 'POST',
            path: '/v0/ValidateRecord/Question',
            config: {
                tags: ['api'],
                description: 'Validate a question json object',
                validate: {
                    payload: Validator.questionSchema,
                },
                response: {
                    status: {
                        200: internals.serverResponse,
                        304: internals.notModified,
                        400: internals.validationError,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => reply({ message: 'Validation Success!' }),
        },
        {
            method: 'POST',
            path: '/v0/ValidateRecord/Study',
            config: {
                tags: ['api'],
                description: 'Validate a study json object',
                validate: {
                    payload: Validator.studySchema,
                },
                response: {
                    status: {
                        200: internals.serverResponse,
                        304: internals.notModified,
                        400: internals.validationError,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => reply({ message: 'Validation Success!' }),
        },
        {
            method: 'POST',
            path: '/v0/ValidateRecord/StudyGroup',
            config: {
                tags: ['api'],
                description: 'Validate a study group json object',
                validate: {
                    payload: Validator.studyGroupSchema,
                },
                response: {
                    status: {
                        200: internals.serverResponse,
                        304: internals.notModified,
                        400: internals.validationError,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => reply({ message: 'Validation Success!' }),
        },
        {
            method: 'POST',
            path: '/v0/ValidateRecord/Variable',
            config: {
                tags: ['api'],
                description: 'Validate a variable json object',
                validate: {
                    payload: Validator.variableSchema,
                },
                response: {
                    status: {
                        200: internals.serverResponse,
                        304: internals.notModified,
                        400: internals.validationError,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => reply({ message: 'Validation Success!' }),
        },
        {
            method: 'GET',
            path: '/v0/ListRecordHeaders/{RecordType?}',
            config: {
                tags: ['api'],
                description: 'Returns list of record headers',
                validate: {
                    headers: internals.cachingAndTracingHeaders,
                    params: {
                        RecordType: Validator.recordTypeSchema,
                    },
                    query: {
                        Repository: Joi.string().required(),
                    },
                },
                timeout: {
                    socket: 60 * 60 * 1000,
                },
                response: {
                    status: {
                        200: Validator.recordHeadersListSchema,
                        304: internals.notModified,
                        400: internals.validationError,
                        404: internals.notFound,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => {
                const repository = request.query.Repository;
                const type = request.params.RecordType;
                const logger = internals.loggerForRequest(request);
                Resolver.resolveRepositoryUrl(options.resolutionServiceUrl, repository, request.headers)
                    .then((url) => Client.listRecordHeadersv0(url, repository, request.headers, logger, type))
                    .then((result) => {
                        internals.setCacheHeaders(reply(result.payload), result.headers)
                            .code(result.statusCode);
                    })
                    .catch((boomError) => reply(boomError));
            },
        },
        {
            method: 'GET',
            path: '/v0/ListSupportedRecordTypes',
            config: {
                tags: ['api'],
                description: 'Returns supported record types',
                validate: {
                    query: {
                        Repository: Joi.string().required(),
                    },
                },
                response: {
                    status: {
                        200: Validator.supportedRecordTypesListSchema,
                        304: internals.notModified,
                        400: internals.validationError,
                        500: internals.internalServerError,
                    },
                },
            },
            handler: (request, reply) => {
                const repository = request.query.Repository;
                const logger = internals.loggerForRequest(request);

                Resolver.resolveRepositoryUrl(options.resolutionServiceUrl, repository, request.headers)
                    .then((url) => Client.listSupportedRecordTypesv0(url, repository, request.headers, logger))
                    .then((result) => {
                        internals.setCacheHeaders(reply(result.payload), result.headers)
                            .code(result.statusCode);
                    })
                    .catch((boomError) => { reply(boomError); });
            },
        },
    ]);

    next();
};

const projectPkg = require('../../package.json');
const pkg = {
    name: 'api',
    version: projectPkg.version,
};

exports.register.attributes = {
    pkg,
};
