FROM node:5.10.1-onbuild

MAINTAINER Snorre Magnus Davøen <snorre.davoen@nsd.no>

CMD [ "npm", "start", "--production" ]

EXPOSE 8080 8081