const Glue = require('glue');
const Package = require('./package.json');

// Load environment variables from .env file. Environment variables from
// .env file does not override environment variables from command line.
// If no .env file is present, suppress warning, .env file should be optional.
require('dotenv').config({ silent: true });
const Joi = require('joi');

const validatePort = (value) => {
    const portValidator = Joi.number().min(0).max(65535);
    return Joi.validate(value, portValidator, (err, val) => {
        if (err) {
            throw err;
        }
        return val;
    });
};

const osmhHost = process.env.OSMH_HOST || 'localhost';
const osmhPort = validatePort(process.env.OSMH_PORT || 8080);

const swaggerScheme = process.env.SWAGGER_SCHEME || 'http';
const swaggerHost = process.env.SWAGGER_HOST || osmhHost;
const swaggerPort = process.env.SWAGGER_PORT || osmhPort;

const internals = {
    manifest: {
        connections: [{
            host: osmhHost,
            port: osmhPort,
            labels: ['api'],
        },
            {
                host: process.env.RESOLUTION_SERVICE_HOST || 'localhost',
                port: process.env.RESOLUTION_SERVICE_PORT || 8081,
                labels: ['resolution-service'],
            }],
        registrations: [
            {
                plugin: {
                    register: 'good',
                    options: {
                        // configuration options:
                        // https://github.com/hapijs/good/blob/642a98b289560465af8e46a07417b9d390e09e32/API.md#options
                        opsInterval: 1000,
                        // requestHeaders: true,
                        // responsePayload: true,
                        reporters: [
                            {
                                reporter: require('./src/json-console'),
                                events: { log: '*', response: '*', request: '*' },
                            },
                        ],
                    },
                },
            },
            { plugin: { register: 'inert' } },
            { plugin: { register: 'vision' } },
            {
                plugin: {
                    register: 'hapi-swagger',
                    options: {
                        schemes: [swaggerScheme],
                        host: `${swaggerHost}:${swaggerPort}`,
                        info: {
                            title: 'Open Source Metadata Harvester API documentation',
                            description: `${Package.name}:${Package.version} - ${Package.description}`,
                            version: `v${Package.version.split('.')[0]}`,
                        },
                    },
                },
            },
            {
                plugin: {
                    register: './src/api-version',
                    options: {
                        select: ['api'],
                        validVersions: [0],
                    },
                },
            },
            {
                plugin: {
                    register: './src/x-request-id',
                    options: {
                        select: ['api', 'resolution-service'],
                    },
                },
            },
            {
                plugin: {
                    register: './src/api',
                    options: {
                        select: ['api'],
                        resolutionServiceUrl: process.env.OSMH_RESOLUTION_SERVICE_URL
                            || 'http://localhost:8081',
                    },
                },
            },
            {
                plugin: {
                    register: './src/resolution-service',
                    options: {
                        select: ['resolution-service'],
                        mappingFile: process.env.RESOLUTION_SERVICE_MAPPINGS || './test-mappings.json',
                        mapping: JSON.parse(process.env.RESOLUTION_SERVICE_MAPPINGS_STR || '{}'),
                    },
                },
            },
        ],
    },
};

module.exports = Glue.compose.bind(Glue, internals.manifest, { relativeTo: __dirname });
