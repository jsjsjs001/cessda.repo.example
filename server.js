const composer = require('./index');

composer((err, pack) => {
    if (err) {
        console.error('Failed to start server.'); // eslint-disable-line no-console
        console.error('server.register err: ', err); // eslint-disable-line no-console
        throw err;
    }
    pack.start((err2) => {
        if (err2) {
            throw err2;
        }
        const apiInfo = pack.select('api').info.uri.toLowerCase();
        const resolutionInfo = pack.select('resolution-service').info.uri.toLowerCase();
        pack.log([], `✅  Server is listening on ${apiInfo}`);
        pack.log([], `✅  Server is listening on ${resolutionInfo}`);
    });
});
